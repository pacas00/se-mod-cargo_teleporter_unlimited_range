﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sandbox.Common;
using Sandbox.Common.Components;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Engine;
using Sandbox.Game;
using Sandbox.ModAPI;
using VRage.ObjectBuilders;
using VRage.Game.Components;
using VRage.ModAPI;
using Sandbox.ModAPI.Interfaces;
using System.Timers;
using System.IO;

namespace CargoTeleporter
{
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_CargoContainer), new string[] { "LargeBlockSmallContainerTeleport", "SmallBlockMediumContainerTeleport" })]
    public class CargoTeleporterServer : MyGameLogicComponent
    {
        MyObjectBuilder_EntityBase ObjectBuilder;
        IMyCubeBlock CargoTeleporter = null;
        Sandbox.ModAPI.IMyInventory inventory = null;
        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            Entity.NeedsUpdate |= MyEntityUpdateEnum.EACH_100TH_FRAME;
            ObjectBuilder = objectBuilder;
            CargoTeleporter = Entity as IMyCubeBlock;
            base.Init(objectBuilder);
        }

        public override MyObjectBuilder_EntityBase GetObjectBuilder(bool copy = false)
        {
            return copy ? ObjectBuilder.Clone() as MyObjectBuilder_EntityBase : ObjectBuilder;
        }

        public override void Close()
        {
            base.Close();
            Logging.close();
                       
        }

        public override void UpdateBeforeSimulation100()
        {
            base.UpdateBeforeSimulation100();
            if (CargoTeleporter == null) return;
            try
            {
                if (MyAPIGateway.Session == null)
                {
                    if (constantStuff.debug) Logging.WriteLine("MyAPIGateway.Session is null");
                    return;
                }
                if (inventory == null) inventory = (Sandbox.ModAPI.IMyInventory)Sandbox.ModAPI.Ingame.TerminalBlockExtentions.GetInventory(CargoTeleporter as IMyTerminalBlock, 0);
                if (CargoTeleporter.BlockDefinition.SubtypeName == "LargeBlockSmallContainerTeleport" || CargoTeleporter.BlockDefinition.SubtypeName == "SmallBlockMediumContainerTeleport")
                {
                    long playerId = CargoTeleporter.OwnerId;
                    if (constantStuff.debug) Logging.WriteLine("" + playerId);
                    try
                    {
                        HashSet<IMyEntity> entities = new HashSet<IMyEntity>();
                        MyAPIGateway.Entities.GetEntities(entities, x => x is IMyCubeGrid);
                        if (constantStuff.debug) Logging.WriteLine("Entitys");
                        HashSet<IMySlimBlock> gridBlocks = new HashSet<IMySlimBlock>();
                        foreach (IMyCubeGrid grid in entities)
                        {
                            List<IMySlimBlock> slim = new List<IMySlimBlock>();
                            grid.GetBlocks(slim, x => x is IMySlimBlock);
                            foreach (IMySlimBlock block in slim)
                            {
                                if (block.FatBlock != null)
                                {
                                    gridBlocks.Add(block);
                                }
                            }
                        }
                        if (constantStuff.debug) Logging.WriteLine("PostGrids");
                        if (constantStuff.debug) Logging.WriteLine("entities " + entities.Count);
                        if (constantStuff.debug) Logging.WriteLine("gridBlocks " + gridBlocks.Count);
                        DateTime startBlockLoop = DateTime.Now;

                        if (!CargoTeleporter.DisplayNameText.Contains("-Off-"))
                        {
                            string name = "";
                            DateTime compStart = DateTime.Now;
                            bool toMode = true;
                            if (CargoTeleporter.DisplayNameText.Contains("[T:"))
                            {
                                int start = CargoTeleporter.DisplayNameText.IndexOf("[T:") + 3;
                                int end = CargoTeleporter.DisplayNameText.IndexOf("]", start);
                                name = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                            }
                            else if (CargoTeleporter.DisplayNameText.Contains("[F:"))
                            {
                                int start = CargoTeleporter.DisplayNameText.IndexOf("[F:") + 3;
                                int end = CargoTeleporter.DisplayNameText.IndexOf("]", start);
                                name = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                                toMode = false;
                            }
                            if (CargoTeleporter.DisplayNameText.Contains("<T:"))
                            {
                                int start = CargoTeleporter.DisplayNameText.IndexOf("<T:") + 3;
                                int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                                name = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                            }
                            else if (CargoTeleporter.DisplayNameText.Contains("<F:"))
                            {
                                int start = CargoTeleporter.DisplayNameText.IndexOf("<F:") + 3;
                                int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                                name = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                                toMode = false;
                            }

                            if (name.Length > 3)
                            {
                                if (constantStuff.debug) Logging.WriteLine("PostName " + name);
                                IMyEntity targetEnt = null;

                                targetEnt = gridBlocks.Where(x => x.FatBlock != null && x.FatBlock.DisplayNameText != null && x.FatBlock.DisplayNameText == name && x.FatBlock.OwnerId == playerId).First().FatBlock;
                                if (targetEnt == null) Logging.WriteLine("targetEnt null");
                                if (targetEnt is IMyCubeBlock)
                                {
                                    IMyCubeBlock targetCube = (IMyCubeBlock)targetEnt;
                                    
                                    Sandbox.ModAPI.IMyInventory inventoryTO = null;
                                    inventoryTO = (Sandbox.ModAPI.IMyInventory)Sandbox.ModAPI.Ingame.TerminalBlockExtentions.GetInventory(targetCube as IMyTerminalBlock, 0);
                                    if (inventoryTO == null)
                                    {
                                        if (targetEnt.GetType().ToString() == "Sandbox.Game.Entities.MyCockpit") return;
                                        Logging.WriteLine("Error. Block didn't return an inventory. Block:" + targetEnt.GetType().ToString());
                                        return;
                                    }
                                    if (!inventoryTO.IsFull && !inventory.Empty() && toMode)
                                    {
                                        inventory.TransferItemTo(inventoryTO, 0, null, true, inventory.GetItems()[0].Amount, false);
                                    }
                                    else if (!inventoryTO.Empty() && !inventory.IsFull && !toMode)
                                    {
                                        inventory.TransferItemFrom(inventoryTO, 0, null, true, inventoryTO.GetItems()[0].Amount, false);
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Logging.WriteLine(ex.Message);
                    }
                }

            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }

        }
    }
}
